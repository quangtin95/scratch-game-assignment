# Scratch Game
## Outline

- [ ] [Technology](#technology)
- [ ] [Installation](#installation)
- [ ] [Requirement](#requirement)
- [ ] [Solution](#solution)

## Technology
Maven version 3.6.3+

Java version 17

## Installation
```
git checkout git@gitlab.com:quangtin95/scratch-game.git
mvn clean install
cd target
java -jar  scratch-game.jar --config <config-file> --betting-amount <betting-amount>
```
* For example:
```
 java -jar scratch-game.jar --config ../config.json --betting-amount 100
```
## Requirement
Please refer to [this link](problem_description.md)

## Solution
I'll be leveraging Java 17, Jackson JSON, and the Apache Common library to develop this game based on the provided requirements. The solution will be structured into five distinct parts:
### 1. Read and validate input
According to the requirements, the application will have two mandatory inputs: the configuration path and the betting amount. If either input is missing, the application will return an error message.
```
Parameter invalid. Please run by command: java -jar <your-jar-file> --config <config-file> --betting-amount <betting-amount>

```

### 2. Read and validate config
After having configuration path, the application will read config file and parse data to [ScratchGameConfiguration](./src/main/java/com/wintex/game/dto/ScratchGameConfiguration.java) dto class. This configuration should be global variable and use whole project. If the configuration is not valid format, incorrect data, it will show error

### 3. Generate matrix
For each cell:
- I will calculate total weight of all symbols in probabilities data
- Pick a random number less than the total
- At each key and value in Map, subtract the weight at that position
- When the result is negative, return that symbol. Please refer to [link](https://www.perlmonks.org/?node_id=242751) 

Pick bonus symbol with same way, and then random row and column to pick a cell in matrix and replace standard to bonus symbol.

Return Matrix model ([SymbolMatrix](./src/main/java/com/wintex/game/dto/SymbolMatrix.java) {matrix, bonusSymbol, hasBonusSymbol})

### 4. Reward calculation
I see that the config game have 2 types: same symbols and linear symbols. Maybe it will have more type in the future. To avoid change code, I used factory pattern to handle for each type.

![alt text](pattern.drawio.png)

#### Linear Symbol:
* For each linear combination
  * For each rows in covered, if the symbol of all columns are the same => store to a Map with key pattern (group_symbol) and value be max reward. Otherwise, the for loop will be break
* Return rewards ([Reward dto](./src/main/java/com/wintex/game/dto/Reward.java)(symbol, winCombinationSymbol, reward))
#### Same symbol

* Scan all cell in matrix and then store a Map with key and value be symbol and count retrospective.
* For each map, if that count in Map is greater than or equal count value in win combinations => store to a Map with key pattern (group_symbol) and value be max reward multiplier
* Return rewards ([Reward dto](./src/main/java/com/wintex/game/dto/))

Collect all rewards from 2 types, calculate total rewards of standard symbols
For bonus symbol at generate matrix step, I can get reward multiplier of bonus symbol data to plus or multiply with total standard symbol reward.


### 5. Print output
Using jackson to write a object to json.
 