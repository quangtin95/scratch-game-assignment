package com.wintex.game;

import lombok.experimental.UtilityClass;

@UtilityClass
public class RandomUtils {
    public int getRandomNumber(int min, int max) {
        return (int) ((Math.random() * (max - min)) + min);
    }

}
