package com.wintex.game.exception;

public class LinearSymbolWrongPatternException extends RuntimeException {
    public LinearSymbolWrongPatternException(String msg){
        super(msg);
    }
}
