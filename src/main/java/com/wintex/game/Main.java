package com.wintex.game;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wintex.game.dto.Reward;
import com.wintex.game.dto.RewardMultiplier;
import com.wintex.game.dto.RewardMultiplierSymbol;
import com.wintex.game.dto.ScratchGameConfiguration;
import com.wintex.game.dto.ScratchGameResult;
import com.wintex.game.dto.StandardSymbol;
import com.wintex.game.dto.SymbolMatrix;
import com.wintex.game.dto.WinCombination;
import com.wintex.game.exception.LinearSymbolWrongPatternException;
import com.wintex.game.reward.LinearSymbolCalculator;
import com.wintex.game.reward.SameSymbolCalculator;
import com.wintex.game.reward.WinCombinationCalculator;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;


public class Main {
    // read parameter
    private final static ObjectMapper objectMapper = new ObjectMapper();
    private final static String CONFIG_FILE_PARAM_NAME = "--config";
    private final static String BETTING_AMOUNT_PARAM_NAME = "--betting-amount";
    private final static List<WinCombinationCalculator> winCombinationCalculators = new ArrayList<>();
    static {
        winCombinationCalculators.add(new SameSymbolCalculator());
        winCombinationCalculators.add(new LinearSymbolCalculator());
    }


    public static void main(String[] args) throws JsonProcessingException {
        try {
            String error = validateInput(args);
            if (StringUtils.isNotBlank(error)) {
                System.out.println(error);
                return;
            }

            String path = args[1];
            BigDecimal betAmount = parseToBigDecimal(args[3]);

            final ScratchGameConfiguration initData = readConfig(path);
            List<String> validations = validateConfig(initData);
            if (CollectionUtils.isNotEmpty(validations)) {
                System.out.println(String.join("\n", validations));
                return;
            }

            SymbolMatrix matrix = generateMatrix(initData);

            // Calculate rewards
            Map<String, Set<Reward>> winRecords = winCombinationCalculators
                    .stream()
                    .map(i -> i.calculator(matrix.getMatrix(), initData)).flatMap(Collection::stream)
                    .collect(groupingBy(Reward::getSymbol, mapping(Function.identity(), toSet())));

            BigDecimal totalReward = BigDecimal.ZERO;
            for (Map.Entry<String, Set<Reward>> entry : winRecords.entrySet()) {
                RewardMultiplier rewardMultiplier = initData.getSymbols().get(entry.getKey());
                BigDecimal rewardBySymbol = BigDecimal.ONE;
                BigDecimal multiply = rewardMultiplier.getRewardMultiplier();
                BigDecimal amt = entry.getValue().stream().map(Reward::getReward).reduce(BigDecimal.ONE, BigDecimal::multiply);
                rewardBySymbol = rewardBySymbol.multiply(betAmount).multiply(multiply).multiply(amt);
                totalReward = totalReward.add(rewardBySymbol);
            }

            // Look up bonus in matrix
            RewardMultiplierSymbol bonus = Optional.of(matrix).filter(SymbolMatrix::isHasBonusSymbol).filter(i -> !winRecords.isEmpty()).map(i -> lookUpBonus(initData, i)).orElse(null);
            if (!Objects.isNull(bonus)) {
                RewardMultiplier rewardMultiplier = bonus.getRewardMultiplier();
                totalReward = rewardMultiplier.getImpact().equals(RewardMultiplier.ImpactType.multiply_reward) ?
                        totalReward.multiply(rewardMultiplier.getRewardMultiplier()) : totalReward.add(rewardMultiplier.getExtra());
            }
            // Return output
            ScratchGameResult result = new ScratchGameResult(matrix.getMatrix(), winRecords, bonus, totalReward);
            System.out.println(objectMapper.writeValueAsString(result));
        } catch (LinearSymbolWrongPatternException ex) {
            System.out.println(ex.getMessage());
        } catch (Exception ex) {
            System.out.println("Something went wrong");
            throw ex;
        }

    }

    private static ScratchGameConfiguration readConfig(String path) {
        try {
            return objectMapper.readValue(new File(path), ScratchGameConfiguration.class);
        } catch (Exception ex ) {
            System.out.println(ex.fillInStackTrace());
            return null;
        }
    }

    private static String validateInput(String[] args) {
        if (args.length != 4 || !CONFIG_FILE_PARAM_NAME.equals(args[0]) || !BETTING_AMOUNT_PARAM_NAME.equals(args[2]) ) {
            String invalid = """
                    Parameter invalid. Please run by command: java -jar <your-jar-file> --config <config-file> --betting-amount <betting-amount>
                    """;
            return invalid;
        }

        if (parseToBigDecimal(args[3]) == null) {
            String error;
            error = "Please fill valid bet amount";
            return error;
        }
        return null;
    }

    private static BigDecimal parseToBigDecimal(String arg) {
        if (StringUtils.isBlank(arg)) {
            return null;
        }
        try {
            double value = Double.parseDouble(arg);
            return new BigDecimal(value);
        } catch (Exception e) {
            return null;
        }
    }

    private static RewardMultiplierSymbol lookUpBonus(ScratchGameConfiguration initData, SymbolMatrix symbolMatrix) {
        RewardMultiplierSymbol rewardMultiplierSymbol = new RewardMultiplierSymbol();
        String bonusSymbol = symbolMatrix.getBonusSymbol();
        rewardMultiplierSymbol.setName(bonusSymbol);
        rewardMultiplierSymbol.setRewardMultiplier(initData.getSymbols().get(bonusSymbol));
       return rewardMultiplierSymbol;
    }

    private static SymbolMatrix generateMatrix(ScratchGameConfiguration initData) {
        String[][] matrix = new String[initData.getRows()][initData.getColumns()];
        SymbolMatrix symbolMatrix = new SymbolMatrix();
        for (int i = 0; i < initData.getRows(); i++) {
            for (int j = 0; j < initData.getColumns(); j++) {
                final int column = j;
                final int row = i;
                Map<String, Integer> symbols =  initData.getProbabilities().getStandardSymbols().stream().filter(ss -> ss.getColumn().equals(column)).filter(ss -> ss.getRow().equals(row)).map(StandardSymbol::getSymbols).findFirst().get();
                String symbol = generateRandomSymbol(symbols);
                matrix[i][j] = symbol;
            }
        }


        String bonusSymbol = generateRandomSymbol(initData.getProbabilities().getBonusSymbols().getSymbols());
        if (!bonusSymbol.equals("MISS")) {
            int randomBonusRow = RandomUtils.getRandomNumber(0, initData.getRows());
            int randomBonusColumn = RandomUtils.getRandomNumber(0, initData.getColumns());
            matrix[randomBonusRow][randomBonusColumn] = bonusSymbol;
            symbolMatrix.setHasBonusSymbol(true);
            symbolMatrix.setBonusSymbol(bonusSymbol);
        }
        symbolMatrix.setMatrix(matrix);

        return symbolMatrix;
    }

    private static String generateRandomSymbol(Map<String, Integer> symbols) {

        int totalWeight = symbols.values().stream().mapToInt(k -> k).sum();
        double r = Math.random() * totalWeight;
        for (Map.Entry<String, Integer> entry : symbols.entrySet()) {
            r -= entry.getValue();
            if (r <= 0) {
                return entry.getKey();
            }
        }
        return symbols.entrySet().stream().min((a,b) -> a.getValue() < b.getValue()? 1 : 0).map(Map.Entry::getKey).get();
    }

    private static List<String> validateConfig(ScratchGameConfiguration initData) {
        List<String> errors = new ArrayList<>();
        if (initData == null) {
            errors.add("Config file path is not exist or invalid");
            return errors;
        }
        if (initData.getRows() == null || initData.getColumns() == null) {
            errors.add("Rows or Columns should be not null");
            return errors;
        }
        boolean [][] matrix = new boolean[initData.getRows()][initData.getColumns()];

        List<StandardSymbol> standardSymbols = initData.getProbabilities().getStandardSymbols();
        StringBuilder unConvered = new StringBuilder();

        for (StandardSymbol standardSymbol : standardSymbols) {
            if (standardSymbol.getRow() >= matrix.length || standardSymbol.getColumn() >= matrix[standardSymbol.getRow()].length) {
                unConvered.append(String.format("[%d:%d]", standardSymbol.getRow(), standardSymbol.getColumn()));
                continue;
            }
            matrix[standardSymbol.getRow()][standardSymbol.getColumn()] = true;
        }
        if (!unConvered.isEmpty()){
            errors.add(String.format("The row and columns of standard symbol in probabilities is not correct, please check position %s", unConvered.toString()));
        }
        StringBuilder covered = new StringBuilder();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (!matrix[i][j]) {
                    covered.append(String.format("[%d:%d]", i, j));
                }
            }
        }
        if (!covered.isEmpty()) {
            errors.add(String.format("The list of standard symbols in probabilities %s are missed ", covered.toString()));
        }

        initData.getWinCombinations().forEach((k, v) -> {
            if (v.getWhen().equals(WinCombination.WhenType.same_symbols)) {
                if (v.getCount() == null) {
                    errors.add(String.format("Count of %s should be not null", k));
                }
                if (v.getRewardMultiplier() == null) {
                    errors.add(String.format("Reward_Multiplier of %s should be not null", k));

                }
            }

            if (v.getWhen().equals(WinCombination.WhenType.linear_symbols)) {
                if (v.getCoveredAreas() == null) {
                    errors.add(String.format("Covered of %s should be not null", k));
                }
                if (v.getRewardMultiplier() == null) {
                    errors.add(String.format("Reward_Multiplier of %s should be not null", k));

                }
            }
        });

        return errors;
    }

}
