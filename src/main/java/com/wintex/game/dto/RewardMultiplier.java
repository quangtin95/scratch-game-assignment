package com.wintex.game.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.Value;

import java.math.BigDecimal;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class RewardMultiplier {
    @JsonProperty("reward_multiplier")
    BigDecimal rewardMultiplier;
    Type type;
    ImpactType impact;
    BigDecimal extra;


    public enum Type {
        standard,
        bonus
    }

    public enum ImpactType {
        multiply_reward,
        extra_bonus,
        miss
    }

}
