package com.wintex.game.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class ScratchGameResult {
    String[][] matrix;
    BigDecimal reward;
    @JsonProperty("applied_winning_combinations")
    Map<String, List<String>> appliedWinningCombinations = new HashMap<>();
    @JsonProperty("applied_bonus_symbol")
    String appliedBonusSymbol;

    public ScratchGameResult(String[][] matrix, Map<String, Set<Reward>> rewards, RewardMultiplierSymbol bonus, BigDecimal totalReward) {
        this.matrix = matrix;
        this.reward = totalReward;
        rewards.forEach((k, v) -> {
            appliedWinningCombinations.put(k, v.stream().map(Reward::getWinCombinationSymbol).toList());
        });
        if (!Objects.isNull(bonus)) {
            appliedBonusSymbol = bonus.name;

        }

    }
}
