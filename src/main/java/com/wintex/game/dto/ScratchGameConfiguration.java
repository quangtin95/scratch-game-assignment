package com.wintex.game.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class ScratchGameConfiguration {
    Integer columns;
    Integer rows;
    Map<String, RewardMultiplier> symbols;
    Probabilities probabilities;
    @JsonProperty("win_combinations")
    Map<String, WinCombination> winCombinations;

}
