package com.wintex.game.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class RewardMultiplierSymbol {
    String name;
    RewardMultiplier rewardMultiplier;
}
