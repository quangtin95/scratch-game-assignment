package com.wintex.game.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.Value;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class WinCombination {
    @JsonProperty("reward_multiplier")
    BigDecimal rewardMultiplier;
    WhenType when;
    GroupType group;
    Integer count;
    @JsonProperty("covered_areas")
    List<List<String>> coveredAreas;

    public enum GroupType{
        same_symbols,
        horizontally_linear_symbols,
        vertically_linear_symbols,
        ltr_diagonally_linear_symbols,
        rtl_diagonally_linear_symbols
    }
    public enum WhenType {
        same_symbols,
        linear_symbols
    }
}
