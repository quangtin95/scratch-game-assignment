package com.wintex.game.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SymbolMatrix {
    private String[][] matrix;
    private boolean hasBonusSymbol;
    private String bonusSymbol;
}
