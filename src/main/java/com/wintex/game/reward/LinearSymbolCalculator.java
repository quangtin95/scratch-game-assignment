package com.wintex.game.reward;

import com.wintex.game.dto.Reward;
import com.wintex.game.dto.ScratchGameConfiguration;
import com.wintex.game.dto.WinCombination;
import com.wintex.game.exception.LinearSymbolWrongPatternException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.wintex.game.dto.WinCombination.WhenType.linear_symbols;
import static org.apache.commons.lang3.StringUtils.EMPTY;

public class LinearSymbolCalculator implements WinCombinationCalculator {
    @Override
    public List<Reward> calculator(String[][] matrix, ScratchGameConfiguration initData) {
        Map<String, Reward> reward = new HashMap<>();

        initData.getWinCombinations().entrySet().stream().filter(i -> i.getValue().getWhen().equals(linear_symbols)).forEach(entry -> {
            WinCombination.GroupType group = entry.getValue().getGroup();
            List<List<String>> covered = entry.getValue().getCoveredAreas();
            covered.forEach(rows -> {
                try {
                    boolean isPass = true;
                    String symbol = EMPTY;
                    for (int i = 0; i < rows.size() - 1; i++) {
                        String[] rc1 = rows.get(i).split(":");
                        String[] rc2 = rows.get(i + 1).split(":");
                        int row1 = Integer.parseInt(rc1[0]);
                        int col1 = Integer.parseInt(rc1[1]);
                        int row2 = Integer.parseInt(rc2[0]);
                        int col2 = Integer.parseInt(rc2[1]);

                        if (!matrix[row1][col1].equals(matrix[row2][col2])) {
                            isPass = false;
                            break;
                        }
                        symbol = matrix[row1][col1];
                    }
                    if (isPass) {
                        BigDecimal rewardAmount = entry.getValue().getRewardMultiplier();
                        Reward newReward = createReward(symbol, entry.getKey(), rewardAmount);
                        reward.compute(String.format("%s_%s", group.name(), symbol), (key, oldValue) -> oldValue == null ? newReward : maxReward(oldValue, newReward));
                    }
                } catch (ArrayIndexOutOfBoundsException ex) {
                    throw new LinearSymbolWrongPatternException(group.name() + " is wrong pattern");
                }


            });
        });
        return new ArrayList<>(reward.values());
    }
}
