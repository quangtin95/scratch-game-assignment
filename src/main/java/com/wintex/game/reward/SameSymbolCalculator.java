package com.wintex.game.reward;

import com.wintex.game.dto.Reward;
import com.wintex.game.dto.ScratchGameConfiguration;
import com.wintex.game.dto.WinCombination;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static com.wintex.game.dto.WinCombination.WhenType.same_symbols;

public class SameSymbolCalculator implements WinCombinationCalculator {
    @Override
    public List<Reward> calculator(String[][] matrix, ScratchGameConfiguration initData) {
        Map<String, Integer> countSymbol = new HashMap<>();
        List<Reward> rewards = new ArrayList<>();

        for (int i = 0 ; i < initData.getRows(); i++) {
            for (int j = 0; j < initData.getColumns(); j++) {
                countSymbol.compute(matrix[i][j], (k, oldValue) -> Objects.isNull(oldValue) ? 1 : oldValue + 1);
            }
        }

        countSymbol.forEach((symbol, count) -> {
            Map<WinCombination.GroupType, Reward> reward = new HashMap<>();

            initData.getWinCombinations().entrySet().stream().filter(i -> i.getValue().getWhen().equals(same_symbols)).forEach(entry -> {
                WinCombination.GroupType name = entry.getValue().getGroup();
                Integer winCount = entry.getValue().getCount();
                if (count >= winCount) {
                    BigDecimal rewardAmount = entry.getValue().getRewardMultiplier();
                    Reward newReward = createReward(symbol, entry.getKey(), rewardAmount);
                    reward.compute(name, (key, oldValue) -> oldValue == null ? newReward : maxReward(oldValue, newReward));
                }

            });
            rewards.addAll(reward.values());

        });
        return rewards;
    }


}
