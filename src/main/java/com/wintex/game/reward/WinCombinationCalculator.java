package com.wintex.game.reward;

import com.wintex.game.dto.Reward;
import com.wintex.game.dto.RewardMultiplier;
import com.wintex.game.dto.RewardMultiplierSymbol;
import com.wintex.game.dto.ScratchGameConfiguration;
import com.wintex.game.dto.WinCombination;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;

public interface WinCombinationCalculator {
    List<Reward> calculator(String[][] matrix, ScratchGameConfiguration initData);

    //    boolean isSupported(Collection<WinCombination> winCombinations);
    default Reward createReward(String nameSymbol, String nameWinCombination, BigDecimal rewardAmount) {
        Reward reward = new Reward();
        reward.setReward(rewardAmount);
        reward.setWinCombinationSymbol(nameWinCombination);
        reward.setSymbol(nameSymbol);
        return reward;
    }
    default Reward maxReward(Reward oldValue, Reward newReward) {
        return oldValue.getReward().compareTo(newReward.getReward()) >= 0 ? oldValue : newReward;
    }
}
